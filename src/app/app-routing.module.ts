import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const appRoutes: Routes = [
  {
    path: 'sign',
    loadChildren: './components/sign-in/sign-in.module#SignInModule'
  },
  {
    path: 'home',
    loadChildren: './components/home/home.module#HomeModule'
  },
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
