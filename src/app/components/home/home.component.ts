import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestService } from '../../services/request.service';
import { TodoListModel } from '../../model/todo-list.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userName: string;
  groupId: string;
  userId: string;
  toDoData: Array<TodoListModel>;
  listForm: FormGroup;

  constructor(private router: Router,
              private fb: FormBuilder,
              private svc: RequestService) {
  }

  ngOnInit() {
    if (!localStorage.getItem('userName')) {
      this.router.navigate(['sign']);
    } else {
      this.userName = localStorage.getItem('userName');
      this.groupId = localStorage.getItem('groupId');
      this.userId = localStorage.getItem('userId');
      this.createForm();
      this.getTodoList();
    }
  }

  createForm() {
    this.listForm = this.fb.group({
      name: [''],
    });
  }

  getTodoList() {
    this.svc.getData(`todos/group/${this.groupId}/user/${this.userId}`).pipe(map(response => {
      return response.body;
    })).subscribe(res => {
      this.toDoData = [];
      const keys = Object.keys(res);
      for (let i = 0; i < keys.length; i++) {
        this.toDoData.push(res[keys[i]]);
      }
    });
  }

  addItem() {
    this.toDoData.push({
      text: this.listForm.value.name,
      done: false
    });
    this.listForm.controls.name.setValue('');
    this.sendRequest(this.toDoData);
  }

  removeItem(i) {
    this.toDoData.splice(i, 1);
    this.sendRequest(this.toDoData);
  }

  doneItem(i) {
    this.toDoData[i].done = true;
    this.sendRequest(this.toDoData);
  }

  sendRequest(data) {
    this.svc.editData(`todos/group/${this.groupId}/user/${this.userId}`, data).subscribe(res => {});
  }

}
