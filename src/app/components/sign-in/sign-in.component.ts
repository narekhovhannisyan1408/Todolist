import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { RequestService } from '../../services/request.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  form: FormGroup;
  loading = false;

  constructor(public snackBar: MatSnackBar,
              private router: Router,
              private fb: FormBuilder,
              private svc: RequestService) {
  }

  ngOnInit() {
    if (localStorage.getItem('userName')) {
      this.router.navigate(['home']);
    } else {
      this.createForm();
    }
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
    });
  }

  createUser() {
    if (this.form.valid) {
      this.loading = true;
      const name = this.form.value.name.toLowerCase();
      const surname = this.form.value.surname.toLowerCase();
      const key = `${name}_${surname}`;
      this.svc.getData(`create/${key}`).pipe(map(response => {
        return response.body.userAdded;
      })).subscribe(res => {
        localStorage.setItem('userName', res);
        this.getGroup(res);
      });
    } else {
      this.snackBar.open('Your form is invalid', 'OK', {
        duration: 2000,
      });
    }
  }

  getGroup(key) {
    this.svc.getData(`group/${key}`).pipe(map(response => {
      return response.body.groupId;
    })).subscribe(res => {
      localStorage.setItem('groupId', res);
      this.getUser(key);
    });
  }

  getUser(key) {
    this.svc.getData(`user/${key}`).pipe(map(response => {
      return response.body.userId;
    })).subscribe(res => {
      localStorage.setItem('userId', res);
      this.loading = false;
      this.router.navigate(['home']);
    });
  }

}
