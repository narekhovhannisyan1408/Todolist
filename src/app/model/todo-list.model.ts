export class TodoListModel {
  constructor(
    public text: string,
    public done: boolean
  ) {}
}
