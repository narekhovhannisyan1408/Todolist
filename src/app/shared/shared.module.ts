import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterComponent } from '../components/footer/footer.component';
import { HeaderComponent } from '../components/header/header.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestService } from '../services/request.service';
import { HttpService } from '../services/http.service';


@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  entryComponents: [],
  providers: [
    RequestService,
    HttpService
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    MaterialModule,
    HeaderComponent,
    FooterComponent
  ],
})
export class SharedModule {
}
