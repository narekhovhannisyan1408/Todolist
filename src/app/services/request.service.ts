import { Injectable } from '@angular/core';

import { HttpService } from './http.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class RequestService {

  constructor(private http: HttpService) {
  }

  getData(url: string, params: any = {}): Observable<any> {
    return this.http.get('/' + url, params);
  }

  addData(url: string, body): Observable<any> {
    return this.http.post('/' + url, body);
  }

  editData(url: string, body): Observable<any> {
    return this.http.put('/' + url, body);
  }

  deleteData(url: string): Observable<any> {
    return this.http.delete('/' + url);
  }
}
